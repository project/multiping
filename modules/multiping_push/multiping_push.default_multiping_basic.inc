<?php

/**
 * Implements hook_default_multiping_basic().
 */
function multiping_push_default_multiping_basic() {
  $targets = array();

  if (module_exists('blog')) {
    $target = new stdClass();
    $target->disabled = TRUE;
    $target->api_version = 1;
    $target->name = 'push_blog_feed';
    $target->admin_title = 'PubSubHubbub Blog Feed';
    $target->admin_description = '';
    $target->method = 'push';
    $target->endpoint = 'https://pubsubhubbub.superfeedr.com/';
    $target->target = 'blog/%user/feed';
    $target->data = array(
      'when_published' => TRUE,
      'when_frontpage' => FALSE,
      'when_nodetype' => TRUE,
      'nodetypes' => array(
        0 => 'blog',
      ),
      'when_vocabulary' => FALSE,
      'when_taxonomy' => FALSE,
    );
    $targets['push_blog_feed'] = $target;
  }

  if (module_exists('taxonomy')) {
    $target = new stdClass();
    $target->disabled = TRUE;
    $target->api_version = 1;
    $target->name = 'push_taxonomy_feed';
    $target->admin_title = 'PubSubHubbub Taxonomy Feed';
    $target->admin_description = '';
    $target->method = 'push';
    $target->endpoint = 'https://pubsubhubbub.superfeedr.com/';
    $target->target = 'taxonomy/term/%term/feed';
    $target->data = array(
      'when_published' => TRUE,
      'when_frontpage' => FALSE,
      'when_nodetype' => FALSE,
      'when_vocabulary' => FALSE,
      'when_taxonomy' => FALSE,
    );
    $targets['push_taxonomy_feed'] = $target;
  }

  $target = new stdClass();
  $target->disabled = TRUE;
  $target->api_version = 1;
  $target->name = 'push_main_feed';
  $target->admin_title = 'PubSubHubbub Main Feed';
  $target->admin_description = '';
  $target->method = 'push';
  $target->endpoint = 'https://pubsubhubbub.superfeedr.com/';
  $target->target = 'rss.xml';
  $target->data = array(
    'when_frontpage' => TRUE,
    'when_published' => TRUE,
    'when_nodetype' => FALSE,
    'when_vocabulary' => FALSE,
    'when_taxonomy' => FALSE,
  );
  $targets['push_main_feed'] = $target;

  return $targets;
}
