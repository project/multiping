<?php

$plugin = array(
  'title' => t('Pubsubhubbub'),
  'description' => t('A method for notifying Pubsubhubbub hubs about updated feeds.'),
  'ping callback' => 'multiping_push_ping',
);

function multiping_push_ping($item) {
  $data = array(
    'hub.mode' => 'publish',
    'hub.url' => $item['target'],
  );

  $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
    'User-Agent' => 'Drupal Multiping (+https://www.drupal.org/project/multiping)',
  );

  $result = drupal_http_request($item['endpoint'], array(
    'method' => 'POST',
    'headers' => $headers,
    'data' => drupal_http_build_query($data),
    'timeout' =>  variable_get('multiping-timeout', 3),
  ));

  if ($result->code != 204) {
    $name = empty($item['name']) ? $item['endpoint'] : $item['name'];

    watchdog(
      'multiping',
      'Pubsubhubbub ping to %site failed with code %code.',
      array(
        '%site' => $name,
        '%code' => $result->code,
      ),
      WATCHDOG_ERROR
    );

    return FALSE;
  }

  return TRUE;
}
