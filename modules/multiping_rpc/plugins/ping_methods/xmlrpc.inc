<?php

$plugin = array(
  'title' => t('XML-RPC'),
  'description' => t('A method for making pings with XML-RPC weblogUpdates.ping.'),
  'ping callback' => 'multiping_rpc_ping',
);

function multiping_rpc_ping($item) {
  $result = _multiping_rpc_ping('weblogUpdates.extendedPing', $item['endpoint'], $item['target']);

  // This replicates how WordPress does things
  if (!$result) {
    $result = _multiping_rpc_ping('weblogUpdates.ping', $item['endpoint'], url('<front>', array('absolute' => TRUE)));
  }

  return $result;
}

function _multiping_rpc_ping($method, $endpoint, $target) {
  $timeout = variable_get('multiping-timeout', 3);

  // Assemble site name.
  $name = variable_get('site_name', '');
  $slogan = variable_get('site_slogan', '');
  if (!empty($slogan)) {
    $name = $name . ' - ' . $slogan;
  }

  return xmlrpc($endpoint, array(
    $method => array($name, $target),
  ), array('timeout' => $timeout));
}
