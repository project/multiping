<?php

/**
 * Implements hook_default_multiping_basic().
 */
function multiping_rpc_default_multiping_basic() {
  $targets = array();

  $target = new stdClass();
  $target->disabled = TRUE;
  $target->api_version = 1;
  $target->name = 'ping_o_matic';
  $target->admin_title = 'Ping-o-matic';
  $target->admin_description = '';
  $target->method = 'xmlrpc';
  $target->endpoint = 'http://rpc.pingomatic.com';
  $target->target = 'rss.xml';
  $target->data = array(
    'when_frontpage' => TRUE,
  );
  $targets['ping_o_matic'] = $target;

  return $targets;
}
