<?php

$plugin = array(
  'schema' => 'multiping_basic',

  'access' => 'administer multiping',

  'menu' => array(
    'menu prefix' => 'admin/config/services/multiping',
    'menu item' => 'multiping_basic',
    'menu title' => 'List – Basic',
    'menu description' => 'Administer basic Multiping targets.',
  ),

  'title singular' => t('target'),
  'title singular proper' => t('Target'),
  'title plural' => t('targets'),
  'title plural proper' => t('Targets'),

  'form' => array(
    'settings' => 'multiping_basic_ctools_export_ui_form',
    'validate' => 'multiping_basic_ctools_export_ui_form_validate',
    'submit' => 'multiping_basic_ctools_export_ui_form_submit',
  ),
);

function multiping_basic_ctools_export_ui_form(&$form, &$form_state) {
  $target = $form_state['item'];

  $methods = multiping_get_ping_methods();

  $method_options = array();
  foreach (multiping_get_ping_methods() as $method) {
    $method_options[$method['name']] = $method['title'];
  }

  $form['method'] = array(
    '#type' => 'radios',
    '#title' => t('Ping method'),
    '#required' => TRUE,
    '#default_value' => $target->method,
    '#options' => $method_options,
  );

  $form['endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint'),
    '#description' => t('The URL of the endpoint to send the ping to.'),
    '#required' => TRUE,
    '#default_value' => $target->endpoint,
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['target'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('The path to notify the endpoint about. To insert a dynamic id, use one of: @tokens', array(
      '@tokens' => implode(', ', _multiping_basic_supported_argument_tokens()),
    )),
    '#required' => TRUE,
    '#default_value' => isset($target->target) ? $target->target : 'rss.xml',
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['when_published'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ping only for published nodes'),
    '#default_value' => isset($target->data['when_published']) ? $target->data['when_published'] : TRUE,
  );

  $form['when_frontpage'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ping only for nodes promoted to front page'),
    '#default_value' => isset($target->data['when_frontpage']) ? $target->data['when_frontpage'] : TRUE,
  );

  $form['when_nodetype'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ping only for selected node types'),
    '#default_value' => isset($target->data['when_nodetype']) ? $target->data['when_nodetype'] : FALSE,
  );

  $form['nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Ping only for nodes of these node types'),
    '#default_value' => isset($target->data['nodetypes']) ? $target->data['nodetypes'] : array(),
    '#options' => node_type_get_names(),
    '#states' => array(
      'visible' => array(
        ':input[name="when_nodetype"]' => array('checked' => TRUE),
      ),
    ),
  );

  if (module_exists('taxonomy')) {
    $form['when_vocabulary'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ping only for selected vocabularies'),
      '#default_value' => isset($target->data['when_vocabulary']) ? $target->data['when_vocabulary'] : FALSE,
    );
    // Vocabularies indexed by machine_name.
    $vocabularies = taxonomy_get_vocabularies();
    $options = array();
    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->machine_name] = $vocabulary->name;
    }
    $form['vocabularies'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Ping only for nodes with terms from these vocabularies'),
      '#default_value' => isset($target->data['vocabularies']) ? $target->data['vocabularies'] : array(),
      '#options' => $options,
      '#states' => array(
        'visible' => array(
          ':input[name="when_vocabulary"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['when_taxonomy'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ping only for selected terms'),
      '#default_value' => isset($target->data['when_taxonomy']) ? $target->data['when_taxonomy'] : FALSE,
      '#states' => array(
        'visible' => array(
          ':input[name="when_vocabulary"]' => array('checked' => TRUE),
          ':input[name^="vocabularies["]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['when_taxonomy_child'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ping also for children of selected terms'),
      '#default_value' => isset($target->data['when_taxonomy_child']) ? $target->data['when_taxonomy_child'] : FALSE,
      '#states' => array(
        'visible' => array(
          ':input[name="when_vocabulary"]' => array('checked' => TRUE),
          ':input[name^="vocabularies["]' => array('checked' => TRUE),
          ':input[name="when_taxonomy"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['terms'] = array(
      '#tree' => TRUE,
    );

    foreach ($vocabularies as $vocabulary) {
      $term_tree = taxonomy_get_tree($vocabulary->vid);
      $term_options = array();
      $term_default_values = array();

      $parent_tids = array();
      $parent_names = array();

      foreach ($term_tree as $term) {
        $last_tid = end($parent_tids);

        while ($last_tid = end($parent_tids) && !in_array($last_tid, $term->parents)) {
          array_pop($parent_tids);
          array_pop($parent_names);
        }

        $parent_tids[] = $term->tid;
        $parent_names[] = $term->name;

        if (isset($target->data['terms']) && in_array($term->tid, $target->data['terms'])) {
          $term_default_values[] = implode(':', $parent_tids);
        }

        $term_options[implode(':', $parent_tids)] = implode(' > ', $parent_names);
      }

      $form['terms'][$vocabulary->machine_name] = array(
        '#type' => 'checkboxes',
        '#title' => t('Ping only for these %vocabulary terms', array('%vocabulary' => $vocabulary->name)),
        '#default_value' => $term_default_values,
        '#options' => $term_options,
        '#states' => array(
          'visible' => array(
            ':input[name="when_vocabulary"]' => array('checked' => TRUE),
            ':input[name="vocabularies[' . $vocabulary->machine_name . ']"]' => array('checked' => TRUE),
            ':input[name="when_taxonomy"]' => array('checked' => TRUE),
          ),
        ),
      );
    }
  }
}

function multiping_basic_ctools_export_ui_form_validate(&$form, &$form_state) {
  if (!valid_url($form_state['values']['endpoint'], TRUE)) {
    form_set_error('endpoint', t("The URL '%url' is invalid.", array('%url' => $form_state['values']['endpoint'])));
  }

  $nodetypes = array_filter($form_state['values']['nodetypes']);
  if (!empty($form_state['values']['when_nodetype']) && empty($nodetypes)) {
    form_set_error('when_nodetype', t("You're filtering by node type, but haven't selected any node types to match against. Select at least one."));
  }

  $vocabularies = array_filter($form_state['values']['vocabularies']);
  if (!empty($form_state['values']['when_vocabulary']) && empty($vocabularies)) {
    form_set_error('when_vocabulary', t("You're filtering by vocabularies, but haven't selected any vocabularies to match against. Select at least one."));
  }

  $terms_values = array();

  foreach ($form_state['values']['terms'] as $vocabulary => $terms) {
    if (!empty($vocabularies[$vocabulary])) {
      foreach ($terms as $tid => $status) {
        if ($status) {
          $tid = explode(':', $tid);
          $tid = array_pop($tid);
          $terms_values[$tid] = TRUE;
        }
      }
    }
  }

  if (!empty($form_state['values']['when_taxonomy']) && empty($terms_values)) {
    form_set_error('when_taxonomy', t("You're filtering by terms, but haven't selected any terms to match against. Select at least one."));
  }

  form_set_value($form['terms'], array_keys($terms_values), $form_state);

  $target_path = $form_state['values']['target'];

  if (
    $target_path !== '<front>' &&
    !url_is_external($target_path) &&
    !menu_get_item($target_path) &&
    !(
      preg_match('/\/%/', $target_path) !== FALSE &&
      db_query(
        "SELECT * FROM {menu_router} where path = :path",
        array(
          ':path' => preg_replace(
            '/\/(\\' . implode('|\\', _multiping_basic_supported_argument_tokens()) . ')/', '/%',
            $target_path
          ),
        )
      )->fetchAssoc()
    )
  ) {
    form_set_error('target', t("The path '%path' is either invalid or you do not have access to it.", array('%path' => $target_path)));
  }
}

function multiping_basic_ctools_export_ui_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];

  $target = $form_state['item'];
  $target->data = array();

  $data_value_keys = array(
    'when_published',
    'when_frontpage',
    'when_nodetype',
    'when_vocabulary',
  );
  foreach ($data_value_keys as $key) {
    $target->data[$key] = !empty($values[$key]);
  }

  $target->data['when_taxonomy'] = $target->data['when_vocabulary'] && !empty($values['when_taxonomy']);

  if ($target->data['when_nodetype']) {
    $target->data['nodetypes'] = array_values(array_filter($values['nodetypes']));
  }
  if ($target->data['when_vocabulary']) {
    $target->data['vocabularies'] = array_values(array_filter($values['vocabularies']));
  }
  if ($target->data['when_taxonomy']) {
    $target->data['when_taxonomy_child'] = !empty($values['when_taxonomy_child']);
    $target->data['terms'] = $values['terms'];
  }

  multiping_cache_clear();
}

function _multiping_basic_token_validation_replacement(&$replacements) {
  $replacements = array_map(function ($replacement) {
    return $replacement === '' ? '%' : $replacement;
  }, $replacements);
}
