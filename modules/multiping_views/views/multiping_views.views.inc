<?php
/**
 * @file
 * Provides the Views integration for Multiping.
 */

/**
 * Implements hook_views_plugins().
 */
function multiping_views_views_plugins() {
  return array(
    'display_extender' => array(
      'multiping' => array(
        'title' => t('Multiping'),
        'help' => t('Provides pinging support for views.'),
        'handler' => 'multiping_views_plugin_display_extender_multiping',
        'enabled' => TRUE,
        'path' => drupal_get_path('module', 'multiping_views') . '/views',
      ),
    ),
  );
}
