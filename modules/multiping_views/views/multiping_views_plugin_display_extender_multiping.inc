<?php
/**
 * @file
 * Custom display extender plugin for Views.
 */

class multiping_views_plugin_display_extender_multiping extends views_plugin_display_extender {

  function options_definition_alter(&$options) {
    $options['multiping'] = array('default' => array());
  }

  /**
   * Defines where within the Views admin UI the new settings will be visible.
   */
  function options_summary(&$categories, &$options) {
    if (
      !_multiping_views_has_supported_table($this->view) ||
      !$this->display->has_path() ||
      !class_exists('views_plugin_display_page') ||
      !($this->display instanceof views_plugin_display_page) ||
      !user_access('administer multiping')
    ) {
      return;
    }

    $option = $this->display->get_option('multiping');
    $option = empty($option) ? array() : $option[0];

    if (isset($option['method']) && !empty($option['endpoint'])) {
      $method = multiping_get_ping_method($option['method']);
      $presentation = $method['title'];
    } else {
      $presentation = t('No');
    }

    $options['multiping'] = array(
      'category' => 'page',
      'title' => t('Ping'),
      'value' => $presentation,
    );
  }

  /**
   * Defines the form.
   */
  function options_form(&$form, &$form_state) {
    if ($form_state['section'] == 'multiping' && user_access('administer multiping')) {
      $form['#title'] .= t('Ping settings for this view');

      $option = $this->display->get_option('multiping');
      $option = empty($option) ? array() : $option[0];

      $methods = multiping_get_ping_methods();

      $method_options = array();
      foreach (multiping_get_ping_methods() as $method) {
        $method_options[$method['name']] = $method['title'];
      }

      $form['method'] = array(
        '#type' => 'radios',
        '#title' => t('Ping method'),
        '#default_value' => isset($option['method']) ? $option['method'] : key($method_options),
        '#options' => $method_options,
      );

      $form['endpoint'] = array(
        '#type' => 'textfield',
        '#title' => t('Endpoint'),
        '#description' => t('The URL of the endpoint to send the ping to.'),
        '#default_value' => isset($option['endpoint']) ? $option['endpoint'] : '',
        '#size' => 60,
        '#maxlength' => 255,
      );
    }
  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) {
    if ($form_state['section'] == 'multiping') {
      if ($form_state['values']['endpoint'] != '' && !valid_url($form_state['values']['endpoint'], TRUE)) {
        form_set_error('endpoint', t("The URL '%url' is invalid.", array('%url' => $form_state['values']['endpoint'])));
      }
    }
  }

  /**
   * Saves the form values.
   */
  function options_submit(&$form, &$form_state) {
    if ($form_state['section'] == 'multiping' && user_access('administer multiping')) {
      $multiping = array_intersect_key($form_state['values'], array_fill_keys(array('method', 'endpoint'), true));

      // Wrap it in an array to prepare for future support for multiple ping targets per view
      $this->display->set_option('multiping', array($multiping));
    }
  }
}
